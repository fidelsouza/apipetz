package com.example.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.model.Pet;

public interface PetRepository extends CrudRepository<Pet,Long> {
	
	List<Pet> findByNome(String nome);
	
	List<Pet> findByDonoId(Long idDono);

}
