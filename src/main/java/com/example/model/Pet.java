package com.example.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.example.enums.PetTipoEnum;

@Entity
public class Pet implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String nome;
	@Enumerated(EnumType.STRING)
	private PetTipoEnum tipo;
	
	@Column(name = "dono_id")
	private Long donoId;
	
	private String raca;
	
	private String caracteristicas;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public PetTipoEnum getTipo() {
		return tipo;
	}
	public void setTipo(PetTipoEnum tipo) {
		this.tipo = tipo;
	}
	
	public String getRaca() {
		return raca;
	}
	public void setRaca(String raca) {
		this.raca = raca;
	}
	public Long getDonoId() {
		return donoId;
	}
	public void setDonoId(Long donoId) {
		this.donoId = donoId;
	}
	public String getCaracteristicas() {
		return caracteristicas;
	}
	public void setCaracteristicas(String caracteristicas) {
		this.caracteristicas = caracteristicas;
	}

}
