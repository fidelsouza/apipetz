package com.example.services;

import com.example.model.Cliente;

public interface ClienteService {
	
	Cliente buscaCliente(String cpf, String rg);
	
	Cliente atualizaCliente(Cliente cliente);
}
