package com.example.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.example.model.Cliente;
import com.example.model.Pet;
import com.example.repository.ClienteRepository;
import com.example.repository.PetRepository;

@Service
public class ClienteServiceImpl implements ClienteService{

	@Autowired
	ClienteRepository clienteRepo;

	@Autowired
	PetRepository petRepo;
	
	@Override
	public Cliente buscaCliente(String cpf, String rg) {

		if (StringUtils.isEmpty(cpf) && StringUtils.isEmpty(rg)) {
			throw new RuntimeException("CPF ou RG deve ser inserido!");
		}

		Cliente cliente = null;
		if (!StringUtils.isEmpty(cpf)) {
			cliente = clienteRepo.findByCpf(cpf);
		}

		else {
			cliente = clienteRepo.findByRg(rg);
		}
		return cliente;
	}
	
	@Override
	public Cliente atualizaCliente(Cliente cliente) {
		
		List<Pet> pets = this.petRepo.findByDonoId(cliente.getId());
		cliente.setPets(pets);

		cliente = this.clienteRepo.save(cliente);
		return cliente;
	}
}
