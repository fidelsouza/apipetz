package com.example.presenter;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.enums.PetTipoEnum;
import com.example.model.Cliente;
import com.example.model.Pet;
import com.example.repository.ClienteRepository;
import com.example.repository.PetRepository;
import com.example.services.ClienteService;

@RequestMapping(value = "/cadastros")
@RestController
public class CadastroPresenter {

	@Autowired
	private ClienteRepository clienteRepo;

	@Autowired
	private PetRepository petRepo;

	@Autowired
	private ClienteService clienteService;

	@GetMapping("/busca-tipos-pet")
	public ResponseEntity<PetTipoEnum[]> buscarTiposPet() {

		return ResponseEntity.status(HttpStatus.OK).body(PetTipoEnum.values());
	}

	@PostMapping("/insere-cliente")
	public ResponseEntity<Cliente> cadastrarCliente(@RequestBody Cliente cliente) {

		cliente = this.clienteRepo.save(cliente);

		return ResponseEntity.status(HttpStatus.OK).body(cliente);
	}

	@GetMapping("/busca-cliente")
	public ResponseEntity<Cliente> buscarCliente(@RequestParam(required = false) String cpf,
			@RequestParam(required = false) String rg) {

		Cliente cliente = this.clienteService.buscaCliente(cpf, rg);

		return ResponseEntity.status(HttpStatus.OK).body(cliente);
	}

	@GetMapping("/busca-todos-clientes")
	public ResponseEntity<Iterable<Cliente>> buscarClientes() {

		Iterable<Cliente> clientes = this.clienteRepo.findAll();

		return ResponseEntity.status(HttpStatus.OK).body(clientes);
	}

	@PutMapping("/atualiza-cliente")
	public ResponseEntity<Cliente> atualizaCliente(@RequestBody Cliente cliente) {

		cliente = this.clienteService.atualizaCliente(cliente);

		return ResponseEntity.status(HttpStatus.OK).body(cliente);
	}

	@DeleteMapping("/remove-cliente")
	public ResponseEntity<String> removeCliente(@RequestParam Long id) {

		Optional<Cliente> cliente = this.clienteRepo.findById(id);

		this.clienteRepo.delete(cliente.get());

		return ResponseEntity.status(HttpStatus.OK).body("Removido");
	}

	@PostMapping("/insere-pet")
	public ResponseEntity<Pet> cadastrarPet(@RequestBody Pet pet) {

		pet = this.petRepo.save(pet);

		return ResponseEntity.status(HttpStatus.OK).body(pet);
	}

	@GetMapping("/busca-pet")
	public ResponseEntity<List<Pet>> buscaPet(@RequestParam String nome) {

		List<Pet> pets = this.petRepo.findByNome(nome);

		return ResponseEntity.status(HttpStatus.OK).body(pets);
	}
	
	@GetMapping("/busca-todos-pets")
	public ResponseEntity<Iterable<Pet>> buscaPets() { 

		Iterable<Pet> pets = this.petRepo.findAll();

		return ResponseEntity.status(HttpStatus.OK).body(pets);
	}

	@PutMapping("/atualiza-pet")
	public ResponseEntity<Pet> atualizaPet(@RequestBody Pet pet) {

		pet = this.petRepo.save(pet);

		return ResponseEntity.status(HttpStatus.OK).body(pet);
	}

	@DeleteMapping("/remove-pet")
	public ResponseEntity<String> removePet(@RequestParam Long id) {

		Optional<Pet> pet = this.petRepo.findById(id);

		this.petRepo.delete(pet.get());

		return ResponseEntity.status(HttpStatus.OK).body("Removido");
	}

}
